angular.module('starter')
.filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}])
.constant('config',{
    'api_url':'http://localhost/armandos/wp-json/wp/v2/'
    //'api_url':'http://gentofte-v2.armandos.dk/wp-json/wp/v2/'
});