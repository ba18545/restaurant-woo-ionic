angular.module('starter.routes', ['ionic', 'starter.controllers','angular-cache'])
.config(function($stateProvider, $urlRouterProvider,CacheFactoryProvider,$ionicConfigProvider) {
    angular.extend(CacheFactoryProvider.defaults, { maxAge: 15 * 60 * 1000 });
    $ionicConfigProvider.tabs.position('bottom');
  $stateProvider

.state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
      controller: 'AppController'
})
.state('app.home', {
    url: '/home',
    views: {
        'tab-home': {
          templateUrl: 'templates/home.html',
          controller: 'HomeController'
        }
      }
})
.state('app.blog', {
  url: '/blog',
  views: {
      'tab-general': {
        templateUrl: 'templates/blog.html',
        controller: 'BlogController'
      }
    }
})
.state('app.contact', {
    url: '/contact',
    views: {
        'tab-general': {
          templateUrl: 'templates/contact.html',
          controller: 'AboutController'
        }
      }
})
.state('app.contact-map', {
  url: '/contact/map',
  views: {
      'tab-general': {
        templateUrl: 'templates/contact-map.html',
        controller: 'AboutController'
      }
    }
})
.state('app.my-account', {
    url: '/my-account',
    cache: false,
    views: {
        'tab-account': {
          templateUrl: 'templates/my-account.html',
          controller: 'AccountController'
        }
      }
})
.state('app.my-account-profile', {
    url: '/my-account/profile',
    cache: false,
    views: {
        'tab-account': {
          templateUrl: 'templates/my-account/profile.html',
          controller: 'ProfileController'
        }
      }
})
.state('app.my-account-orders', {
    url: '/my-account/orders',
    views: {
        'tab-account': {
          templateUrl: 'templates/my-account/orders.html',
          controller: 'OrderController'
        }
      }
})
.state('app.my-account-orders-detail', {
    url: '/my-account/orders/:id',
    views: {
        'tab-account': {
          templateUrl: 'templates/my-account/order-detail.html',
          controller: 'OrderDetailController'
        }
      }
})
.state('app.my-account-billing-address', {
    url: '/my-account/billing-address',
    views: {
        'tab-account': {
          templateUrl: 'templates/my-account/billing-address.html',
          controller: 'AddressController'
        }
      }
})
.state('app.my-account-shipping-address', {
    url: '/my-account/shipping-address',
    views: {
        'tab-account': {
          templateUrl: 'templates/my-account/shipping-address.html',
          controller: 'AddressController'
        }
      }
})
.state('app.categories', {
    url: '/categories',
    views: {
        'tab-menu': {
          templateUrl: 'templates/categories.html',
          controller: 'CategoryController'
        }
      }
})
.state('app.cart', {
    url: '/cart',
    views: {
        'tab-cart': {
          templateUrl: 'templates/cart.html',
          controller: 'CartController'
        }
      }
})
.state('app.checkout', {
    url: '/cart/checkout',
    views: {
        'tab-cart': {
          templateUrl: 'templates/checkout.html',
          controller: 'CheckoutController'
        }
      }
})
  .state('app.menu', {
    url: '/categories',
    views: {
      'tab-categories': {
        templateUrl: 'templates/categories.html',
        controller: 'CategoryController'
      }
    }
})
.state('app.single-category', {
    url: '/categories/:categoryId/:categoryName',
    views: {
      'tab-menu': {
        templateUrl: 'templates/products.html',
        controller: 'CategoryController'
      }
    }
})
.state('app.single-product', {
    url: '/categories/product/:productId/:productName',
    views: {
      'tab-menu': {
        templateUrl: 'templates/product.html',
        controller: 'ProductController'
      }
    }
});
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});