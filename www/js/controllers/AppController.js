angular.module('starter.controllers', [])
.controller('AppController',['$scope','$state','$window','$ionicHistory','$location','CART','$rootScope','$ionicModal','API','User', function($scope,$state,$window,$ionicHistory,$location,CART,$rootScope,$ionicModal,API,User) {
    $rootScope.itemsCounter = CART.getCounter();
    $rootScope.cartTotal = CART.getTotal();
    if(User.getToken()){
        API.getMe(User.getToken()).then(function(res){
            console.log(res);
        },function(err){
            if(err.data.status == 403){
                User.logout();
            }
        })
    }
    $ionicModal.fromTemplateUrl('templates/terms.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });
    $scope.cart= function(event){
        if($ionicHistory.currentStateName() != 'app.cart'){
            
            $ionicHistory.nextViewOptions({
                disableBack: true,
                disableAnimate: true,
            });
            
            $state.go('app.cart');
        }
    }
    
    $scope.home = function(){
        if($ionicHistory.currentStateName() != 'app.home'){
            
            $ionicHistory.nextViewOptions({
                disableBack: true,
                disableAnimate: true,
            });
            
            $state.go('app.home');
        }
    }
    $scope.account = function(){
        if($ionicHistory.currentStateName() != 'app.my-account'){
            
            $ionicHistory.nextViewOptions({
                disableBack: true,
                disableAnimate: true,
            });
            
            $state.go('app.my-account');
        }
    }
    $scope.terms = function(){
        API.getTerms().then(function(response){
            $scope.termsContent = response;
          },function(err){
              console.log(err);
          });
        $scope.modal.show();
    }    
}]);