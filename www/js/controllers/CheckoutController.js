angular.module('starter.controllers')

.controller('CheckoutController', function($scope, $ionicModal, $timeout,API,$ionicLoading,$state,$ionicHistory,User,Order,CART,$ionicPopup,$filter) {
  $scope.show = false;

    $ionicModal.fromTemplateUrl('templates/register.html', {
        scope: $scope,
        animation:'slide-in-right'
    }).then(function(modal) {
        $scope.registerModal = modal;
    });

  $ionicModal.fromTemplateUrl('templates/terms.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.checkoutForm = {};
  if(User.isLoggedIn()){
    $scope.show = true;
    $scope.user = User.getUser();
  } else {
    $scope.user = {};
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope,
        backdropClickToClose: false
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
  } 
  $scope.paymentType= {checked : 'cod'};
  
  $scope.loginData = {};
  $scope.registerData = {};
  $scope.paymentTypes = [];
  API.getPayments(true).then(function(response){
      $scope.paymentTypes = response;
      $scope.paymentType.checked = response[0].id;
    },function(err){
        console.log(err);
    });
  $scope.doPay=function(form) {
    if(form.$valid) {
      var payment = _.find($scope.paymentTypes,function(item){
        return item.id == $scope.paymentType.checked;
      });
      var shippingObject = CART.getShippingObject();
      var data = {
        payment_method: payment.id,
        payment_method_title: payment.title,
        shipping_lines:[{
          method_id: shippingObject.id,
          method_title: shippingObject.title,
          total: parseInt(shippingObject.cost)
        }]
      }
      var order = new Order(CART.getItems(),$scope.user,data);

      if($scope.paymentType.checked == 'epay_dk'){
        $ionicLoading.show({template:'Loading...'});
        order.createOrder(function(err,response){
          if(err){
            $ionicLoading.show({template:'Order Failed',duration:1000});
          } else {
            $ionicLoading.hide();
            openEpay(response,function(err,cb){
              if(err){
                $ionicLoading.show({template:'Payment Failed',duration:1000});
              } else {
                CART.clearCart();
                $scope.modal.hide(); 
                $ionicLoading.show({template:'Proccesing'});
                setTimeout(function(){
                  $ionicLoading.hide();
                  $ionicLoading.show({template:'Your order is placed',duration:1000});
                  $state.go('app.categories');
                  setTimeout(function(){
                    $ionicHistory.clearHistory();
                    location.reload("");
                  },1000);
                  
                },2000);
              }
            });
          }
        });
        
      } else {
        $ionicLoading.show({template:'Proccesing...'});
        order.createOrder(function(err,response){
          $ionicLoading.hide();
          if(err){
            $ionicLoading.show({template:'Order Failed',duration:1000});
          } else {
            $ionicLoading.show({template:'Your order is placed',duration:1000});
            CART.clearCart();
            $ionicHistory.clearHistory();
            $state.go('app.categories');
            }
        });
      }
    } else {
        var alertPopup = $ionicPopup.alert({
            title: $filter('translate')('FILL_REQUIRED'),
            template: null,
            buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
                text: $filter('translate')('CONTINUE'),
                type: 'button-default',
                onTap: function(e) {
                    alertPopup.close();
                }
            }]
        });
    }
  }
  $scope.doLogin=function() {
    API.userLogin($scope.loginData.username,$scope.loginData.password).then(function(response){
      $scope.user = User.getUser();
      $scope.modal.hide();
    },function(err){
      alert(err.message,'Error');
      $scope.show = false;
    });
  }
  $scope.check = function(){
    if($scope.paymentType.checked==undefined){
      $scope.paymentType.checked = $scope.paymentTypes[0].id;
    }
    
  };

    $scope.register=function() {
        $scope.registerModal.show();
    };
    $scope.doRegister= function(form){
        if(form.$valid) {
            API.userRegister($scope.registerData).then(function(response){
                $scope.registerModal.hide();
                $scope.modal.hide();
                $scope.show = true;
                $scope.user = User.getUser();
            },function(err){
                alert(err.message,'Error');
            })
        } else {
            console.log("invalid form");
        }
    };
    $scope.closeRegister = function() {
        $scope.registerModal.hide();
    };

  $scope.closeLogin = function() {
    $ionicHistory.goBack();
    $scope.modal.hide();
  };
  function openEpay(data,cb){
    $ionicModal.fromTemplateUrl('templates/epay.html', {
      scope: $scope,
      backdropClickToClose: false
    }).then(function(modal) {
      $scope.modal = modal;
      
      $scope.modal.show();
      paymentwindow = new PaymentWindow({
        'merchantnumber': "8025894",
        'amount': data.total.split('.')[0] + '00',
        'paymentcollection': "1",
        'currency': "DKK",
        'orderid': data.id,
        'timeout' : "1",
        'mobile': "0",
        'windowstate': "4",
        'iframewidth':"100%",
        "language": 1
    });
    paymentwindow.append('payment-div');
    
    paymentwindow.on('completed', function(params){ 
      delete paymentwindow; 
      cb(null,params);
    });
    paymentwindow.open();          
    });
  }
  $scope.openTerms = function(){
    API.getTerms().then(function(response){
      $scope.termsContent = response;
    },function(err){
        console.log(err);
    });
  $scope.modal.show();
  }


})