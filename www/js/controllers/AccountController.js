angular.module('starter.controllers')
.controller('AccountController',['$scope','$state','$ionicModal','User','API','CacheFactory', function($scope,$state,$ionicModal,User,API,CacheFactory) {
    $scope.loginData = {};
    $scope.registerData = {};
    $scope.show = false;
    if(User.isLoggedIn()){
      $scope.show = true;
    } else {
      $ionicModal.fromTemplateUrl('templates/login.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
          $scope.modal.show();
        });
    }
  $ionicModal.fromTemplateUrl('templates/register.html', {
    scope: $scope,
      animation:'slide-in-right'
    }).then(function(modal) {
        $scope.registerModal = modal;
  });
  $scope.doLogin=function() {
    API.userLogin($scope.loginData.username,$scope.loginData.password).then(function(response){
      $scope.modal.hide();
      $scope.show = true;
    },function(err){
      alert(err.message,'Error');
      $scope.show = false;
    });
  }
  $scope.register=function() {
    $scope.registerModal.show();
  }
  $scope.doRegister= function(form){
    if(form.$valid) {
      API.userRegister($scope.registerData).then(function(response){
        $scope.registerModal.hide();
        $scope.modal.hide();
        $scope.show = true;
      },function(err){
        alert(err.message,'Error');
      })
    } else {
      console.log("invalid form");
    }
  }
  $scope.closeLogin = function() {
    $scope.modal.hide();
    $state.go('app.home');
  };
  $scope.closeRegister = function() {
    $scope.registerModal.hide();
  };
  $scope.logout = function(){
    User.logout();
      $state.go('app.home');
  }
}]);