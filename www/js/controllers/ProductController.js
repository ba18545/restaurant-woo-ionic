angular.module('starter.controllers')

.controller('ProductController', function($scope,$stateParams, $ionicModal, $timeout,API,CART,$ionicPopup,$state,Product,$ionicScrollDelegate,$filter) {
/*    var product = new Product($stateParams.productId);
  product.getProduct().then(function(product){
    console.log(product);
  },function(err){
    console.log(err);
  })*/
  $scope.addonsSelected=[];
  $scope.isVariation = false;
  $scope.hasExtra = false;
  $scope.productName = $stateParams.productName;
  $scope.currentProduct = API.currentProduct;
  $scope.productPrice = Number(API.currentProduct.price);
  $scope.isShown = false;
  $scope.variation= {checked : ''};
  $scope.iconClass="icon ion-arrow-down-b";
  $scope.quantity = 1;
  if(!$scope.productPrice){
    $scope.isVariation = true;
    $scope.hasExtra =false;
  } else {
    $scope.hasExtra =true;
  }
  API.getProductAddons($stateParams.productId).
          then(function(response){
            console.log(response);
                $scope.addons = response;
                $scope.variation.checked = response[0].value;
                if($scope.isVariation){
                    var currentPrice = Number($scope.currentProduct.price);
                    var newPrice    = currentPrice + Number($scope.addons[0].price);
                    $scope.currentProduct.price = newPrice.toString();
                    $scope.productPrice = $scope.quantity * newPrice;
                }
            },function(err){
                console.log(err);
            });
  
  $scope.addonPrice = function($event,price){
    var checkbox = $event.target;
    if(checkbox.checked){
      $scope.addonsSelected.push(checkbox.value);
      var currentPrice = Number($scope.currentProduct.price);
      var newPrice    = currentPrice + Number(price);
      $scope.currentProduct.price = newPrice.toString();
      $scope.productPrice = $scope.quantity * newPrice;
    } else {
      var currentPrice = Number($scope.currentProduct.price);
      var newPrice    = currentPrice - Number(price);
      $scope.currentProduct.price = newPrice.toString();
      $scope.productPrice = $scope.quantity * newPrice;
      if ($scope.addonsSelected.indexOf(checkbox.value) > -1) {
          $scope.addonsSelected.splice($scope.addonsSelected.indexOf(checkbox.value), 1);
      }
    }
  }
  $scope.variationPrice = function($event,price){
    var checkbox = $event.target;
    $scope.currentProduct.price = 0;
    $scope.productPrice = 0;
    $scope.addonsSelected= [];

    if($scope.variation.checked==undefined){
      $scope.variation.checked = $scope.addons[0].value;
      $scope.addonsSelected.push($scope.variation.checked);

    var currentPrice = Number($scope.currentProduct.price);
    var newPrice    = currentPrice + Number($scope.addons[0].price);
    $scope.currentProduct.price = newPrice.toString();
    $scope.productPrice = $scope.quantity * newPrice;


    } else {
      $scope.addonsSelected.push($scope.variation.checked);
        var currentPrice = Number($scope.currentProduct.price);
        var newPrice    = currentPrice + Number(price);
        $scope.currentProduct.price = newPrice.toString();
        $scope.productPrice = $scope.quantity * newPrice;
    }
    
    
  }
  $scope.incQty = function () {
      $scope.quantity++;
      $scope.productPrice += Number($scope.currentProduct.price);
  }
  $scope.decQty = function () {
      if($scope.quantity >1){
        $scope.quantity--;
        $scope.productPrice -= Number($scope.currentProduct.price);
      }
      
  }
  $scope.addToCart = function () {
      if(!_.isEmpty($scope.addonsSelected)){
        API.currentProduct.addons = $scope.addonsSelected;
      }
      CART.addItem(API.currentProduct,$scope.quantity);
      this.showAlert();
      
  }
  $scope.showExtras = function(){
      $scope.isShown = !$scope.isShown;
      if(!$scope.isShown){
          $scope.iconClass="icon ion-arrow-down-b";
          $ionicScrollDelegate.resize();
      } else {
          $scope.iconClass="icon ion-arrow-up-b";
          $ionicScrollDelegate.resize();
      }
  }
  $scope.showAlert = function() {
       var alertPopup = $ionicPopup.alert({
         title: $filter('translate')('PRODUCT_ADDED_TO_CART'),
         template: null,
         buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
            text: $filter('translate')('CONTINUE'),
            type: 'button-default',
            onTap: function(e) {
              alertPopup.close();
            }
          }, {
            text: $filter('translate')('GO_TO_CART'),
            type: 'button-positive',
            onTap: function(e) {
              // Returning a value will cause the promise to resolve with the given value.
              $state.go('app.cart');
            }
          }]
       });
     };
})