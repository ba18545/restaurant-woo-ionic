angular.module('starter.controllers')
.controller('CategoryController',['$scope','$state','$stateParams',"API",'CART','$ionicPopup','CacheFactory', function($scope,$state,$stateParams,API,CART,$ionicPopup,CacheFactory) {
    if($stateParams.categoryId){
        $scope.categoryId = $stateParams.categoryId;
        $scope.addToCart = function(product){
            CART.addItem(product);
            this.showAlert();
        };
        $scope.setCurrentProduct = function(product){
            console.log(product);
            API.currentProduct = product;
        }
        $scope.categoryName = $stateParams.categoryName;
        API.getProductsByCategory($stateParams.categoryId)
            .then(function(response){
                $scope.products = response;
            },function(err){
                console.log(err);
            });
        
    } else {
        API.getCategories()
            .then(function(response){
                $scope.categories = response;
            },function(err){
                console.log(err);
            });
    }
    $scope.getProduct=function(el){
        console.log(el);
    }
    $scope.refresh = function(){
        if($stateParams.categoryId){
        $scope.categoryName = $stateParams.categoryName;
        API.getProductsByCategory($stateParams.categoryId)
            .then(function(response){
                $scope.products = response;
            },function(err){
                console.log(err);
            }).finally(function(){
                    $scope.$broadcast('scroll.refreshComplete');
                });
        
        } else {
            API.getCategories(true)
                .then(function(response){
                    $scope.categories = response;
                },function(err){
                    console.log(err);
                }).finally(function(){
                    $scope.$broadcast('scroll.refreshComplete');
                });
        }
    }
    
    // An alert dialog
     $scope.showAlert = function() {
       var alertPopup = $ionicPopup.alert({
         title: 'Product added to cart',
         template: null,
         buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
            text: 'Cancel',
            type: 'button-default',
            onTap: function(e) {
              alertPopup.close();
            }
          }, {
            text: 'Go To Cart',
            type: 'button-positive',
            onTap: function(e) {
              // Returning a value will cause the promise to resolve with the given value.
              $state.go('app.cart');
            }
          }]
       });
     };
}]);