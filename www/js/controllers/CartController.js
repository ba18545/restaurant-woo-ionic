angular.module('starter.controllers')
.controller('CartController',['$scope','CART','$ionicListDelegate','API', function($scope,CART,$ionicListDelegate,API) {
    $scope.shipping_method= {checked : null};
    
    $scope.items = CART.getItems();
    $scope.shippingTotal = 0;
    $scope.shipping = _.sumBy($scope.items, function(o) {
        return Number(o.price * o.quantity);
    });
    $scope.removeItem = function(index){
        CART.removeItem(index);
        $scope.items = CART.getItems();
        $ionicListDelegate.closeOptionButtons();
    }
    $scope.fireEvent=function () {
        $scope.total = "111111";
    }
    $scope.clear=function(){
        $scope.items = null;
        CART.clearCart();
    }
    API.getShipping(true).then(function(response){

        if(response.length > 0){
            var methods = _.orderBy(response,function(item){
                return item.cost;
            })
            $scope.shipping_methods = methods;
            if(CART.getShippingMethod()){
                console.log('cart',CART.getShippingMethod());
                $scope.shipping_method.checked = CART.getShippingMethod();
                var obj = _.find($scope.shipping_methods,function(o){
                    return o.id == CART.getShippingMethod();
                })
                $scope.shippingTotal = obj.cost;
            } else {
                CART.setShippingObject(methods[0]);
                CART.setShippingMethod(methods[0].id);
                $scope.shipping_method.checked = methods[0].id;
                $scope.shippingTotal = methods[0].cost;
            }
            
        } else {
            $scope.shipping_method = null;
            $scope.shipping_methods = null;
            $scope.shippingTotal = 0;
        }
        
      },function(err){
          console.log(err);
      });
    $scope.check = function(id){
        $scope.shipping_method.checked = id;
        var obj = _.find($scope.shipping_methods,function(o){
            return o.id == $scope.shipping_method.checked;
        })
        CART.setShippingMethod($scope.shipping_method.checked);
        CART.setShippingObject(obj);
        $scope.shippingTotal = obj.cost;
    }
}]);