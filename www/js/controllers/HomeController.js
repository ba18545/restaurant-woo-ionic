angular.module('starter.controllers')

.controller('HomeController', function($scope,$state, $ionicModal, $timeout,API,Product,$ionicViewSwitcher,Shop) {
    $scope.isOpen = function(){
        return Shop.isOpen();
    }
    $scope.goToCategories = function(){
        //$ionicViewSwitcher.nextDirection('forward');
        $state.go('app.categories');
    }
})