angular.module('starter.controllers')

.controller('AboutController', function($scope, $ionicModal, $timeout,API,Product,$ionicLoading,$state) {
    $scope.hgt = window.innerHeight - 80;
    if($state.current.name == "app.contact-map"){
        $ionicLoading.show({ template: 'Loading...' });
    }
    $scope.sendData = {};
    window.uploadDone=function(){
        $ionicLoading.hide();
    }
    $scope.send = function(form){
        if(form.$valid){
            API.sendMessage($scope.sendData).then(function(response){
                if(response){
                    $ionicLoading.show({ template: 'Message Sent!' });
                    setTimeout(function(){
                        $ionicLoading.hide();
                    },1000);
                } else {
                    $ionicLoading.show({ template: 'Cannot send message!?' });
                    setTimeout(function(){
                        $ionicLoading.hide();
                    },1000);
                }
            },function(err){
                $ionicLoading.show({ template: err });
                setTimeout(function(){
                    $ionicLoading.hide();
                },1000);
            });
        }
    }
})