angular.module('starter.controllers')

.controller('ProfileController', function($scope, $ionicModal, $timeout,API, User,Popup,$ionicLoading,$filter) {
  $scope.user = User.getUser();
  $scope.update_profile = function(form){
    if(form.$valid) {
      var user = {
        first_name : $scope.user.first_name,
        last_name: $scope.user.last_name
      }
      API.updateProfile(user,User.getToken()).then(function(success){
        $ionicLoading.show({ template: $filter('translate')('UPDATE_PROFILE_SUCCESS'),duration:1000 });
        $scope.user = User.getUser();

      })
    } else {
      
    }
  }
})