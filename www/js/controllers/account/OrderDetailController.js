angular.module('starter.controllers')

.controller('OrderDetailController', function($scope, $ionicModal, $timeout,API, User,$state,$stateParams) {
  API.getOrders(false,User)
    .then(function(orders){
        var order = _.find(orders,{id:Number($stateParams.id)});
       $scope.order = order;
    },function(err){
        console.log(err);
    });
})