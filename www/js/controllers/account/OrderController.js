angular.module('starter.controllers')

.controller('OrderController', function($scope, $ionicModal, $timeout,API, User,$state) {
  API.getOrders(true,User)
  .then(function(success){
      $scope.orders = success;
  },function(err){
      console.log(err);
  })
  $scope.refresh = function(){
        API.getOrders(true,User)
        .then(function(success){
            $scope.orders = success;
        },function(err){
            console.log(err);
        }).finally(function(){
                    $scope.$broadcast('scroll.refreshComplete');
                });
  }
  $scope.go = function ( id ) {
        $state.go('app.my-account-orders-detail',{id:id});
    };
})