angular.module('starter.controllers')

.controller('AddressController', function($scope, $ionicModal, $timeout,API, User,Popup) {
  $scope.user = User.getUser();
  $scope.update_address = function(form){
    if(form.$valid) {
      var address = {
        billing_first_name : $scope.user.billing.first_name,
        billing_last_name:  $scope.user.billing.last_name,
        billing_address_1:  $scope.user.billing.address_1,
        billing_phone : $scope.user.billing.phone
      }
      API.updateAddress(address,User.getToken()).then(function(success){
        if(success){
          $ionicLoading.show({ template: $filter('translate')('UPDATE_ADDRESS_SUCCESS'),duration:1000 });
          $scope.user = User.getUser();
        }
        
      })
    } else {
      Popup.show();
    }
  }
})