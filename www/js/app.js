// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.routes','tmh.dynamicLocale','ngCordova','pascalprecht.translate'])
    .constant('availableLanguages',['en-US','da-Dk'])
    .constant('defaultLanguage','da-Dk')
.run(function($ionicPlatform,tmhDynamicLocale,$translate,$cordovaGlobalization,availableLanguages,$rootScope,defaultLanguage,$locale,API) {
    API.getConfig(true).then(function(conf){
        $rootScope.config = conf;
    });
    function applyLanguage(language) {
        tmhDynamicLocale.set(language.toLowerCase());
    }

    function getSuitableLanguage(language) {
        for (var index = 0; index < availableLanguages.length; index++) {
            if (availableLanguages[index].toLowerCase() === language.toLocaleLowerCase()) {
                return availableLanguages[index];
            }
        }
        return defaultLanguage;
    }

    function setLanguage() {
        if (typeof navigator.globalization !== "undefined") {
            $cordovaGlobalization.getPreferredLanguage().then(function (result) {
                var language = getSuitableLanguage(result.value);
                applyLanguage(language);
                $translate.use(language);
            });
        } else {
            applyLanguage(defaultLanguage);
        }
    }
  $ionicPlatform.ready(function() {
    setLanguage();
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.backgroundColorByHexString("#67483d");
    }
  });
})
    .config(function (tmhDynamicLocaleProvider, $translateProvider, defaultLanguage) {
        tmhDynamicLocaleProvider.localeLocationPattern('locales/angular-locale_{{locale}}.js');
        $translateProvider.useStaticFilesLoader({
            'prefix': 'i18n/',
            'suffix': '.json'
        });
        $translateProvider.preferredLanguage(defaultLanguage);
    })
