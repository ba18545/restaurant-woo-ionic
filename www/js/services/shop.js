angular.module('starter')
    .factory('Shop', function ($window, CacheFactory,API,$rootScope) {
        var config = CacheFactory.get('config');
        
        var weekday = new Array(7);
        weekday[0] =  "sun";
        weekday[1] = "mon";
        weekday[2] = "tue";
        weekday[3] = "wed";
        weekday[4] = "thu";
        weekday[5] = "fri";
        weekday[6] = "sat";

        
        var Shop = {
            isOpen:function(){
                var d = new Date();
                var time = d.getHours() + ':' + d.getMinutes();

                var day = weekday[d.getDay()];
                try {
                    if(config && config.get('config')){
                        var conf = config.get('config');
                        var key = Object.keys(conf.working_hours[day])[0];
                        var currentDay = conf.working_hours[day][key];

                        if(time >= currentDay.open && time <= currentDay.close){
                            return true;
                        } else {
                            return false;
                        }

                    }
                    else
                        return false;
                } catch(e){
                    return false;
                }
            }
        }
        return Shop;

    });