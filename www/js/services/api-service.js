angular.module('starter')
    .factory('API', ['$http', '$q', '$ionicLoading', 'config', 'CacheFactory',  function ($http, $q, $ionicLoading, config, CacheFactory) {
        var categoriesCache, postsCache, userCache, productAddonsCache, ordersCache,productsCache,termsCache,configCache;
        if (!CacheFactory.get('categories')) {
            categoriesCache = CacheFactory('categories', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('posts')) {
            postsCache = CacheFactory('posts', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('user')) {
            userCache = CacheFactory('user', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('addons')) {
            productAddonsCache = CacheFactory('addons', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('orders')) {
            ordersCache = CacheFactory('orders', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('products')) {
            productsCache = CacheFactory('products', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('shipping')) {
            shippingCache = CacheFactory('shipping', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('terms')) {
            termsCache = CacheFactory('terms', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('payments')) {
            paymentsCache = CacheFactory('payments', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        if (!CacheFactory.get('config')) {
            configCache = CacheFactory('config', {
                storageMode: 'localStorage' // This cache will use `localStorage`.
            });
        }
        var API = {
            getCategories: function (forceRefresh) {
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                var allCategories = null;
                if (!forceRefresh) {
                    allCategories = categoriesCache.get('all');
                }
                if (allCategories) {
                    console.log('Fetching categories from cache');
                    deffered.resolve(categoriesCache.get('all'));
                } else {
                    $ionicLoading.show({ template: 'Loading...' });
                    console.log('Fetching categories from http');
                    $http.get(config.api_url + 'products/categories')
                        .success(function (response) {
                            categoriesCache.put('all', response);
                            $ionicLoading.hide();
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            getProducts: function (forceRefresh) {
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                var allProducts = null;
                if (!forceRefresh) {
                    allProducts = productsCache.get('all');
                }
                if (allProducts) {
                    console.log('Fetching products from cache');
                    deffered.resolve(productsCache.get('all'));
                } else {
                    console.log('Fetching products from http');
                    $http.get(config.api_url + 'products')
                        .success(function (response) {
                            productsCache.put('all', response);
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            getProductsByCategory: function (id, forceRefresh) {
                var deffered = $q.defer();
                if (categoriesCache.get(id)) {
                    console.log('Fetching product categories from cache');
                    deffered.resolve(categoriesCache.get(id));
                } else {
                    console.log('Fetching product categories from http');
                    $ionicLoading.show({ template: 'Loading...' });
                    $http.get(config.api_url + 'products?category=' + id)
                        .success(function (response) {
                            $ionicLoading.hide();
                            categoriesCache.put(id, response);
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            getProductAddons: function (id, forceRefresh) {
                var deffered = $q.defer();
                if (productAddonsCache.get(id)) {
                    console.log('Fetching product addons from cache');
                    deffered.resolve(productAddonsCache.get(id));
                } else {
                    console.log('Fetching product addons from http');
                    $ionicLoading.show({ template: 'Loading...' });
                    $http.get(config.api_url + 'products/' + id + '/addons')
                        .success(function (response) {
                            $ionicLoading.hide();
                            productAddonsCache.put(id, response);
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            getPosts: function (forceRefresh) {
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                var allPosts = null;
                if (!forceRefresh) {
                    allPosts = postsCache.get('all');
                }
                if (allPosts) {
                    console.log('Fetching posts from cache');
                    deffered.resolve(postsCache.get('all'));
                } else {
                    $ionicLoading.show({ template: 'Loading...' });
                    console.log('Fetching posts from http');
                    $http.get(config.api_url + 'posts?_embed')
                        .success(function (response) {
                            postsCache.put('all', response);
                            $ionicLoading.hide();
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            userLogin: function (username, password) {
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                $http.post(config.api_url + 'authentication/login', { username: username, password: password }, { handleError: true })
                    .success(function (response) {
                        userCache.put('auth', response);
                        userCache.put('profile', response.profile);
                        $ionicLoading.hide();
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        $ionicLoading.hide();
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            userRegister: function (data) {
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                $http.post(config.api_url + 'authentication/register', data, { handleError: true })
                    .success(function (response) {
                        userCache.put('auth', response);
                        userCache.put('profile', response.profile);
                        $ionicLoading.hide();
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        $ionicLoading.hide();
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            userProfile: function (token) {
                var req = {
                    method: 'GET',
                    url: config.api_url + 'customers/me',
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                };
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                $http(req)
                    .success(function (response) {
                        userCache.put('profile', response);
                        $ionicLoading.hide();
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            createOrder: function (data,User) {
                var token = User.getToken();
                var req = {
                    method: 'POST',
                    url: config.api_url + 'orders',
                    data: data,
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                };
                var deffered = $q.defer();
                $http(req)
                    .success(function (response) {
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            getOrders: function (forceRefresh,User) {
                var token = User.getToken();
                var req = {
                    method: 'GET',
                    url: config.api_url + 'orders',
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                };
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                var allOrders = null;
                if (!forceRefresh) {
                    allOrders = ordersCache.get('all');
                }
                if (allOrders) {
                    console.log('Fetching orders from cache');
                    deffered.resolve(ordersCache.get('all'));
                } else {
                    $ionicLoading.show({ template: 'Loading...' });
                    console.log('Fetching orders from http');
                    $http(req)
                        .success(function (response) {
                            ordersCache.put('all', response);
                            $ionicLoading.hide();
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            getShipping : function(forceRefresh){
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                if(!forceRefresh && shippingCache.get('all')){
                    console.log('Fetching shipping methods from cache');
                    $ionicLoading.hide();
                    deffered.resolve(shippingCache.get('all'));
                } else {
                    console.log('Fetching shipping methods from http');
                    $http.get(config.api_url + 'general/shipping_methods')
                        .success(function (response) {
                            shippingCache.put('all',response)
                            deffered.resolve(response);
                            $ionicLoading.hide();
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }
                
                
                return deffered.promise;
            },
            getPayments : function(forceRefresh){
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });

                if(!forceRefresh && paymentsCache.get('all')){
                    console.log('Fetching payment methods from cache');
                    deffered.resolve(paymentsCache.get('all'));
                    $ionicLoading.hide();
                } else {
                    console.log('Fetching payment methods from http');
                    $http.get(config.api_url + 'general/payment_gateways')
                        .success(function (response) {
                            $ionicLoading.hide();
                            paymentsCache.put('all',response);
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }
                
                return deffered.promise;
            },
            getTerms : function(){
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                if(termsCache.get("terms")){
                    console.log('Fetching terms and conditions from cache');
                    deffered.resolve(termsCache.get("terms"));
                    $ionicLoading.hide();
                } else {
                    console.log('Fetching terms and conditions from http');
                    $http.get(config.api_url + 'pages?slug=terms-and-condition')
                        .success(function (response) {
                            $ionicLoading.hide();
                            termsCache.put('terms', response[0]);
                            deffered.resolve(response[0]);
                        })
                        .error(function (err) {
                            $ionicLoading.hide();
                            deffered.reject(err);
                        });
                }
                
                return deffered.promise;
            },
            sendMessage:function(data){
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Sending...' });
                $http.post(config.api_url + 'general/contact', data, { handleError: true })
                    .success(function (response) {
                        $ionicLoading.hide();
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        $ionicLoading.hide();
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            getMe:function(token){
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                var req = {
                    method: 'GET',
                    url: config.api_url + 'authentication/me',
                    headers: {
                        'Authorization': 'Bearer ' + token
                    }
                };
                $http(req, { handleError: true })
                    .success(function (response) {
                        $ionicLoading.hide();
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        if(err.data.status == 403){
                            $ionicLoading.show({ template: 'Session Expired! <br>Please Login Again!' ,duration:3000});
                        }
                        
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            getConfig:function(forceRefresh){
                if (typeof forceRefresh === 'undefined') { forceRefresh = false; }
                var deffered = $q.defer();
                var conf = null;
                if (!forceRefresh) {
                    conf = configCache.get('config');
                }
                if (conf) {
                    console.log('Fetching config from cache');
                    deffered.resolve(configCache.get('config'));
                } else {
                    console.log('Fetching config from http');
                    $http.get(config.api_url + 'general/config')
                        .success(function (response) {
                            configCache.put('config', response);
                            deffered.resolve(response);
                        })
                        .error(function (err) {
                            deffered.reject(err);
                        });
                }

                return deffered.promise;
            },
            updateProfile:function(data,token){
                var req = {
                    method: 'POST',
                    url: config.api_url + 'authentication/profile',
                    headers: {
                        'Authorization': 'Bearer ' + token
                    },
                    data:data
                };
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                $http(req, { handleError: true })
                    .success(function (response) {
                        $ionicLoading.hide();
                        userCache.put('profile', response);
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        $ionicLoading.hide();
                        deffered.reject(err);
                    });
                return deffered.promise;
            },
            updateAddress:function(data,token){
                var req = {
                    method: 'POST',
                    url: config.api_url + 'authentication/address',
                    headers: {
                        'Authorization': 'Bearer ' + token
                    },
                    data:data
                };
                var deffered = $q.defer();
                $ionicLoading.show({ template: 'Loading...' });
                $http(req, { handleError: true })
                    .success(function (response) {
                        $ionicLoading.hide();
                        userCache.put('profile', response);
                        deffered.resolve(response);
                    })
                    .error(function (err) {
                        $ionicLoading.hide();
                        deffered.reject(err);
                    });
                return deffered.promise;

            },
            currentProduct: {}
        }
        return API;
    }]);