angular.module('starter')
    .factory('User', function($window,CacheFactory,API) {
    
    var User = {
        isLoggedIn:function(){
            try {
                var customer = CacheFactory.get('user');
                if(customer && customer.get('auth'))
                    return true;
                else
                    return false;
            } catch(e){
                return false;
            }
            
        },
        getUser:function() {
            var customer = CacheFactory.get('user');
            return customer.get('profile');
        },
        getToken:function(){
            try{
                var customer = CacheFactory.get('user');
                return customer.get('auth').token;
            } catch(e) {
                return false;
            }

        },
        doLogin:function(username,password) {
            
        },
        updateAddress:function(data){
                
        },
        updateProfile:function(data){
            API.updateProfile(data).then(function(success){
                if(success){
                    console.log(success);
                }
            })
        },
        logout:function(){
            userCache = CacheFactory.get('user');
            userCache.removeAll();
        }
    }
    
    return User;
});