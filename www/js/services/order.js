angular.module('starter')
    .factory('Order', function ($window, CacheFactory,API,User) {
        var Order = function (cart, user, data) {
            this.cart = cart;
            this.user = user;
            this.data = data;
            this.createOrder = function (response) {
                var req = prepareOrder(this.cart,this.user,this.data);
                API.createOrder(req,User).then(function(success){
                    response(null,success);
                },function(err){
                    response(err);
                });
            }
        }
        function prepareOrder(cart, user, data) {
            data.shipping_lines[0].total = data.shipping_lines[0].total.toString();
            var request = {
                    payment_method: data.payment_method,
                    payment_method_title: data.payment_method_title,
                    billing: user.billing,
                    shipping: user.shipping,
                    shipping_lines:data.shipping_lines,
                    line_items: []
                }
            if (!_.isEmpty(cart)) {
                cart.forEach(function(item){
                    request.line_items.push({product_id:item.id,quantity:item.quantity,addons:item.addons});
                });
                
            } else {
                console.log("Cart is empty");
            }

            return request;
        }
        return Order;

    });