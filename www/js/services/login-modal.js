angular.module('starter')
    .factory('LoginModal', function ($window, CacheFactory,API,$ionicModal) {
        var LoginModal = {
            open:function () {
                $ionicModal.fromTemplateUrl('templates/login.html', {
                    scope: $scope
                }).then(function(modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
            }
        }
        return LoginModal;

    });