angular.module('starter')
    .factory('CART', ['$window','$rootScope', function($window,$rootScope,API) {

        var CART = {
            items:{},
            shipingMethod: null,
            shippingObject:null,
            getShippingObject: function(){
                return this.shippingObject;
            },
            setShippingObject: function(obj){
                this.shippingObject = obj;
            },
            getShippingMethod: function(){
                return this.shipingMethod;
            },
            setShippingMethod:function(method){
                this.shipingMethod = method;
            },
            addItem:function(product,quantity){
                this.items = {};
                var quantity = typeof quantity !== 'undefined' ?  quantity : 1;
                if(!product.addons) { product.addons = []};
                product.quantity = quantity;
                var exist = false;
                var arr = [];
                var items = this.getItems();
                console.log('Before Items',items);
                if(items){
                    items.forEach(function(item){
                        if(item.id == product.id && _.isEqual(item.addons.sort(), product.addons.sort())){
                            console.log('Equal:' + item.addons.sort() + ' ' + product.addons.sort());
                            item.quantity += quantity;
                            $rootScope.itemsCounter = $rootScope.itemsCounter +quantity;
                            exist = true;
                            $window.localStorage.setItem("cart",JSON.stringify(items));
                        }
                    });
                    if(!exist){
                        console.log('not exist');
                        if($rootScope.itemsCounter){
                            $rootScope.itemsCounter = $rootScope.itemsCounter +quantity;
                        } else {
                            $rootScope.itemsCounter = quantity;
                        }
                        items.push(product);
                        $window.localStorage.setItem("cart",JSON.stringify(items));
                    }

                } else {
                    console.log('No items');
                    if($rootScope.itemsCounter){
                        $rootScope.itemsCounter = $rootScope.itemsCounter +quantity;
                    } else {
                        $rootScope.itemsCounter = quantity;
                    }
                    product.quantity = quantity;
                    arr.push(product);
                    $window.localStorage.setItem("cart",JSON.stringify(arr));
                }
                console.log('After Items',this.getItems());
                $rootScope.cartTotal = this.getTotal();
            },
            getItems:function(){
                if(_.isEmpty(this.items)){
                    console.log('Returning cart items from angular cache');
                    return this.items=JSON.parse($window.localStorage.getItem("cart"));
                } else {
                    console.log('Returning cart items from object');
                    return this.items;
                }

            },
            removeItem:function(index){
                var items = this.getItems();
                var product = items[index];
                items.splice(index,1);

                $rootScope.itemsCounter = $rootScope.itemsCounter-product.quantity;
                $window.localStorage.setItem("cart",JSON.stringify(items));
                this.items = JSON.parse($window.localStorage.getItem("cart"));
                $rootScope.cartTotal = this.getTotal();
            },

            getCounter:function(){
                var items = this.getItems();
                if(items){
                    var sum = _.sumBy(items, function(o) {
                        return Number(o.quantity);
                    });
                    return sum;
                } else {
                    return null;
                }

            },
            getTotal:function(){
                var items = this.getItems();
                if(items){
                    var sum = _.sumBy(items, function(o) {
                        return Number(o.price * o.quantity);
                    });
                    return sum;

                } else {
                    return null;
                }
            },
            clearCart:function(){
                $window.localStorage.removeItem('cart');
                this.items = {};
                $rootScope.itemsCounter = null;
            }
        }

        return CART;
    }]);