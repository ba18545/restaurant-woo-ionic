angular.module('starter')
    .factory('Popup', function($window,CacheFactory,$ionicPopup,$filter) {
    
    var Popup = {
        show:function(){
            var alertPopup = $ionicPopup.alert({
            title: $filter('translate')('PRODUCT_ADDED_TO_CART'),
            template: null,
            buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
                text: $filter('translate')('CONTINUE'),
                type: 'button-default',
                onTap: function(e) {
                  alertPopup.close();
                }
              }, {
                text: $filter('translate')('GO_TO_CART'),
                type: 'button-positive',
                onTap: function(e) {
                  // Returning a value will cause the promise to resolve with the given value.
                  alertPopup.close();
                }
              }]
          });
        },
        success:function(message){
          var alertPopup = $ionicPopup.alert({
          title: message,
          template: null
          
            });
          }
    }
    
    return Popup;
});