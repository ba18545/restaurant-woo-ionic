angular.module('starter')
    .factory('Product', function ($window, CacheFactory, API) {
        var Product = function (id) {
            this.id = id;
        }
        Product.prototype.getProduct = function () {
            var self = this;
            return API.getProducts().then(function(products){
                var p = _.find(products,{id:Number(self.id)});
                if(p){
                    return p;
                } else {
                    throw "Cannot find product";
                }
                
            },function(err){
                throw "Cannot find products";
            });
        };
        return Product;
    });